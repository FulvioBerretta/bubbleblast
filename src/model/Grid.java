package model;

import java.util.*;
public class Grid {



    private static final int numberOfRows = 6;
    private static final int numberOfColumns = 5;

    private Bubble[][]  frame = generateGrid();
    private Bubble[][] copyOfFrame = copyGrid(frame);


    /**
     * metodo utilizzato per gnerare il frame di gioco
     */
    public Bubble[][] generateGrid(){

        Bubble[][] grid = new Bubble[numberOfRows][numberOfColumns];
        for (int row = 0; row < numberOfRows; row++){
            for (int column = 0; column < numberOfColumns; column ++){
                grid[row][column] = new Bubble(Bubble.selectRandom(), row, column);
            }
        }
        return grid;
    }


    /**
     * metodo che copia la griglia istanziando nuovi spazi di memoria per le bolle
     */
    protected Bubble[][] copyGrid(Bubble[][] grid){
        Bubble[][] bubbles = new Bubble[numberOfRows][numberOfColumns];
         Arrays.stream(grid).forEach(b -> Arrays.stream(b)
                .forEach(bubble -> {
                   bubbles[bubble.getRiga()][bubble.getColonna()] = Bubble.bubbleClone(bubble);
                }));
         return bubbles;

    }


    /**
     * Metodo che stampa la griglia
     */

    public void printFrame(){
        Arrays.stream(frame)
                .forEach(riga ->{
                    Arrays.stream(riga)
                            .forEach(elem -> System.out.print(elem.getStatus().getSimbolo()));
                    System.out.println();
                } );
        System.out.println("---------------");


    }

    /**

     * Questo metodo calcola la potenza della bolla passata al metodo.
     *  Per fare ciò viene assegnto un valore intero alla bolla, che varia a seconda dello
     *  stato in cui si trova.
     * si ritorna un valore intero pari alla somma dei valori:
     * -della bolla in questione
     * -delle bolle vicine a questa
     * - delle bolle vicine delle bolle vicine
     */

    public int calculateStrength(Bubble b, Bubble[][] selectedFrame){
        int powerCount = 0;
        Bubble[] nearBubbles = findNearBubbles(b, selectedFrame);

        List<Bubble>  nearOfNearBubbles = new ArrayList<>();

            for(Bubble bb: nearBubbles){
                for(int k = 0; k < 4; k++){
                    if(bb != null) nearOfNearBubbles.add(findNearBubbles(bb,selectedFrame)[k]);
                }

            }

        powerCount = Arrays.stream(nearBubbles)
                .filter(bubble -> bubble != null)
                .mapToInt(bubble -> bubble.getStatus().getPower())
                .sum();

        powerCount += nearOfNearBubbles.stream()
                .filter(bubble -> bubble != null)
                .mapToInt(bubble -> bubble.getStatus().getPower())
                .sum();

        powerCount += b.getStatus().getPower();

        return powerCount;
    }

    /**
     * Questo metodo utilizza il metodo calculateStrenght su ogni bolla del Frame,
     * individuando così fra tutte quella migliore da far esplodere
     */
    public Bubble bestMove( Bubble[][] selectedFrame){
        Bubble bestBubble = selectedFrame[0][0];
        for (int row = 0; row < numberOfRows; row++){
            for (int column = 0; column < numberOfColumns; column ++){
                if(calculateStrength(selectedFrame[row][column], selectedFrame) >= calculateStrength(bestBubble, selectedFrame)){
                    bestBubble = selectedFrame[row][column];
                }
            }
        }
        return bestBubble;
    }

    /**
     * ritorna il numero minimo di mosse da effettuare per vincere
     * su una copia della griglia di gioco. Per far ciò si conta quante volte
     * bisogna chiamare chiamare il metodo che ci permette di far esplodere una bolla, sulla bolla
     * individuata come migliore.
     */
    public int minimumMoves(){

        int numbOfMOves = 0;

        Bubble bestBubble = bestMove(copyOfFrame);
        while(stillToExplode(copyOfFrame)){
            inputControlAndStartReaction(bestBubble.getRiga(), bestBubble.getColonna(), copyOfFrame, true);
            numbOfMOves++;
        }
        return numbOfMOves;

    }

    /**
     * Questo metodo viene chiamato dal main ogni volta che l'utente
     * deve inserire le coordinate della bolla che intende selezionare
     */
    public void selectBubble(){

        Scanner input = new Scanner(System.in);
        System.out.println("Inserisci un valore per la riga...");
        int row = input.nextInt();
        System.out.println("Ed ora uno per la colonna...");
        int column = input.nextInt();

        if(!getByCoordinates(row,column,frame).isExploded()){
            inputControlAndStartReaction(row, column, frame, false);
        } else {
            System.out.println("La bolla selezionata e' gia' esplosa");
            selectBubble();
        }
        Test.stringBuilder.append("La bolla selezionata ha riga " + row + " e colonna " + column);


    }

    /**
     * precondizione: vengono passate le cordinate intere della bolla
     * post-condizione: il metodo dopo aver controllato la validità dell'input procede a
     * chiamare il metodo che da il via alla reazione
     */
    public  void inputControlAndStartReaction(int row, int column, Bubble[][] selectedFrame, boolean itIsSimulated) {
        if ((row >= 0 && row < numberOfRows) && ((column >= 0 && column < numberOfColumns))) { //controllo degli input da console
            startReaction(selectedFrame[row][column], selectedFrame, itIsSimulated);
        } else {
            System.out.println("Immettere valori validi per righe e colonne");
        }
    }

    /**
     * precondizione: il metodo riceve in input una matrice valida di bolle
     * post-condizione: il metodo ritorna true se e solo se almeno
     * una delle bolle presenti nella matrice non è ancora esplosa, false altrimenti
     */
    protected  boolean stillToExplode(Bubble[][] selectedframe){

        return Arrays.stream(selectedframe)
                .flatMap(Arrays::stream)
                .anyMatch(bubble -> !bubble.isExploded());

    }

    /**
     * Il metodo prende in input, oltre ad un boolean per verificare se ci troviamo nel
     * caso di simulazione (usato per stimare il numero di mosse minimo necessari al giocatore
     * per terminare la partita), una bolla e la griglia di riferimento.
     * Il metodo:
     * 1. prende le bolle vicine alla bolla passata in input
     * 2. cambia lo stato della bolla in questione
     * 3. Se nel cambio di stato la bolla e' esplosa
     *   3b. cambio lo stato alle bolle vicino
     *   3c. se nel cambio di stato, qualcuna delle bolle vicine è esplosa chiamo ricorsivamente
     *   il metodo startReaction
     */
    private void startReaction(Bubble b, Bubble[][] selectedFrame, boolean itIsSimulated){
        Bubble[] nearBubbles = findNearBubbles(b, selectedFrame);
        b.changeState();

        if(!itIsSimulated) printFrame();

        if(b.isExploded()){
            Arrays.stream(nearBubbles)
                    .filter(bubble -> bubble!= null && stillToExplode(selectedFrame))
                    .forEach(
                        (bubble) -> {
                            bubble.changeState();
                            if(bubble.isExploded()){
                                startReaction(bubble, selectedFrame, itIsSimulated);
                            }
                    });
        }

    }

    /**
     * pre-condizione: viene passata come input al metodo, oltre che il frame di riferimento
     * un bolla non nulla.
     * post-condizione: il metodo restituisce un array composto dalle 4 bolle non esplose
     * vicine alla bolla in questione.
     */

    public Bubble[] findNearBubbles(Bubble b, Bubble[][] selectedFrame){
        Bubble[] bubblesInRow = new Bubble[numberOfColumns];
        Bubble[] bubblesInColumn = new Bubble[numberOfRows];
        Bubble[] nearBubbles = new Bubble[4];

        for(int i = 0; i < numberOfColumns; i++){
            bubblesInRow[i] = selectedFrame[b.getRiga()][i];
        }

        for(int i = 0; i < numberOfRows; i++){
            bubblesInColumn[i] = selectedFrame[i][b.getColonna()];
        }

        int  columnOfRightElement = Arrays.stream(bubblesInRow)
                .filter(y -> y != null)
                .filter(w -> !w.isExploded())
                .mapToInt(Bubble::getColonna)
                .filter(z -> z > b.getColonna()) //essendo l'elemento a destra la sua colonna dev'essere maggiore della colonna della bolla passata al metodo
                .findFirst()
                .orElse(-1);

        nearBubbles[0] = columnOfRightElement != -1 ? getByCoordinates(b.getRiga(),columnOfRightElement, selectedFrame): null;



        int columnOfLeftElement = Arrays.stream(bubblesInRow)
                .filter(y -> y != null)
                .filter(w -> !w.isExploded())
                .filter( z -> z.getColonna() < b.getColonna())
                .mapToInt(Bubble::getColonna)
                .max() //di tutti gli elementi a sinistra prendo quello on il valore della colonna più alto, ovvero il più vicino alla bolla in esame
                .orElse(-1);

        nearBubbles[1] = columnOfLeftElement != -1? getByCoordinates(b.getRiga(),columnOfLeftElement, selectedFrame): null;

        int rowOfElementBelow = Arrays.stream(bubblesInColumn)
                .filter(y -> y != null)
                .filter(w -> !w.isExploded())
                .filter( z -> z.getRiga() > b.getRiga())
                .mapToInt(Bubble::getRiga)
                .findFirst()
                .orElse(-1);

        nearBubbles[2] = rowOfElementBelow != -1? getByCoordinates(rowOfElementBelow, b.getColonna(), selectedFrame): null;

        int rowOfElementAbove = Arrays.stream(bubblesInColumn)
                .filter(y -> y != null)
                .filter(w -> !w.isExploded())
                .filter( z -> z.getRiga() < b.getRiga())
                .mapToInt(Bubble::getRiga)
                .max()
                .orElse(-1);

        nearBubbles[3] = rowOfElementAbove != -1 ? getByCoordinates(rowOfElementAbove, b.getColonna(), selectedFrame): null;

        return nearBubbles;
    }


    public Bubble getByCoordinates(int riga, int colonna, Bubble[][] selectedFrame){
        if ((riga >= 0 && riga < numberOfRows) && ((colonna >= 0 && colonna < numberOfColumns))) {
            return selectedFrame[riga][colonna];
        } else {
            System.out.println("Inseriti dei valori di riga e/o colonna non validi");
            selectBubble();
        }
        return null;
    }


    public Bubble[][] getFrame() {
        return frame;
    }


}
