package model;

import java.io.*;

public class Printer {

    private static FileWriter fileWriter;
    private static Printer instance;

    //Singleton
    public static Printer getInstance() throws  IOException{
        if(instance == null){
            instance = new Printer();
            return instance;
        } else {
            return instance;
        }
    }

    /**
     * Inizializzazione del fileWriter con il nome del file
     */
    private Printer() throws IOException{
        fileWriter = new FileWriter("elencoMosse.txt");
        fileWriter.write("Elenco Mosse\n");

    }

    /**
    * questo metodo si occupa di appendere la griglia di gioco allo stringBuilder
     */
    public static void appendFrame(Grid grid) throws IOException{
        if(Test.stringBuilder != null){
            for (int riga = 0; riga < 6; riga++){
                for (int colonna = 0; colonna < 5; colonna ++){
                    Test.stringBuilder.append(grid.getFrame()[riga][colonna].getSymbol().getSimbolo());
                }
                Test.stringBuilder.append("\n");
            }
            Test.stringBuilder.append("------------").append("\n");

        }

    }

    /**
     * Questo metodo appende la descrizione della mossa allo stringBuilder
     */
    public static void appendMoveDescription(int riga, int colonna) throws  IOException{
        String output = "E' stata selezionata la bolla alla riga: " + riga + " e alla colonna: " + colonna;
        OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream("elencoMosse.txt", true));
        if(fileWriter!= null){
            out.append(output);
            out.append("\n");
            out.close();
        }



    }

    public static FileWriter getFileWriter() {
        return fileWriter;
    }


}
