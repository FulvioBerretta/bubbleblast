package model;

import java.util.Random;

public class Bubble {
    private Stati symbol;
    private int riga;
    private int colonna;


    /**
     * Ad ogni bolla viene associato un simbolo,
     * un nome ed una potenza direttamente
     * nell'enum
     */
    public enum Stati {
        NEAR_EXPLOSION("nearExplosion", " N ", 3),
        HALF_INFLATED("halfInflated", " H ", 2),
        NOT_INFLATED("notInflated", " - ", 1),
        NO_MORE("noMore", " X ", 0);

        private final String nome;
        private final String simbolo;
        private final int power;

        /*Costruttore dell'enum*/
         Stati(String nome, String simbolo, int power){
            this.nome = nome;
            this.simbolo = simbolo;
            this.power = power;
        }

        public String getNome(){
             return this.nome;
        }

        public String getSimbolo(){
             return this.simbolo;
        }

        public int getPower(){
             return this.power;
        }
    }
    /**
     * costruttore della classe bolla
     */
    public Bubble(Stati symbol, int riga, int colonna) {
        this.symbol = symbol;
        this.riga = riga;
        this.colonna = colonna;
    }

    public static Stati selectRandom(){
        Stati[] listBubble = {Stati.HALF_INFLATED, Stati.NEAR_EXPLOSION, Stati.NOT_INFLATED};
        int randomInt = new Random().nextInt(3);
        return listBubble[randomInt];
    }

    /**
     * Questo metodo cambia lo stato della bolla facendolo passare allo stato successivo
     */
    public void changeState(){

        if(symbol.equals(Stati.NEAR_EXPLOSION)){
            this.symbol = symbol.NO_MORE;
        } else if (symbol.equals(Stati.HALF_INFLATED)){
            this.symbol = symbol.NEAR_EXPLOSION;
        } else if (symbol.equals(Stati.NOT_INFLATED)){
            this.symbol = symbol.HALF_INFLATED;
        }
    }

    public static Bubble bubbleClone(Bubble b){
        Stati stato = b.getStatus();
        int x = b.getRiga();
        int y = b.getColonna();
        return new Bubble(stato,x, y);
    }



    public Stati getStatus(){
        return symbol;
    }

    public int getRiga() {
        return riga;
    }

    public int getColonna() {
        return colonna;
    }

    public Stati getSymbol() {
        return symbol;
    }

    /**
     * ritorna True se la bolla in questione è esplosa
     */
    public boolean isExploded(){
        return this.getStatus().getSimbolo().equals(Bubble.Stati.NO_MORE.getSimbolo());
    }

    /**
     * ritorna True se la bolla in questione è sgonfia
     */
    public boolean isNotInflated(){
        return this.getStatus().getSimbolo().equals(Bubble.Stati.NOT_INFLATED.getSimbolo());
    }

    /**
     * ritorna True se la bolla in questione è prossima all'esplosione
     */
    public boolean isNearExplosion(){
        return this.getStatus().getSimbolo().equals(Bubble.Stati.NEAR_EXPLOSION.getSimbolo());
    }

    /**
     * ritorna True se la bolla in questione è gonfia a metà
     */
    public boolean isHalfInflated(){
        return this.getStatus().getSimbolo().equals(Bubble.Stati.HALF_INFLATED.getSimbolo());
    }

}
