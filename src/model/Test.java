package model;

import Utilities.Printer;

import java.io.FileWriter;
import java.io.IOException;


public class Test {
    public static StringBuilder stringBuilder = new StringBuilder();

    public static void main(String[] args) {
        Grid grid = new Grid();
        Bubble[][] myFrame = grid.getFrame();
        //------------------------------------------------------------------
        int minimumMoves = grid.minimumMoves();
        System.out.println("The minimum number of moves is " + minimumMoves);
        stringBuilder.append("The minimum number of moves is " + minimumMoves + "\n");
        //------------------------------------------------------------------

        try {
            Printer printer = Printer.getInstance();
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileWriter fw = Printer.getFileWriter();

        grid.printFrame(); //stampo la griglia dando la possibilità all'utente di visionarla
        try {
            Printer.appendFrame(grid);
        } catch (IOException e) {
            e.printStackTrace();
        }

        /** pre-condizione: presenza di una griglia generata correttamente
         * post-condizione: il gioco termina quando la griglia ha
         * solo bolle nello stato esplose
         *
         */
        while(grid.stillToExplode(myFrame)){

            grid.selectBubble();

            if( stringBuilder != null){

                stringBuilder.append("\n");
                try {
                    Printer.appendFrame(grid);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }

        System.out.println(stringBuilder);

        /** Appendo ciò che è presente nello stringbuilder all'interno del file
         *  specifica nel fileWriter, ovvero nel costruttore della classe Printer
         *  che ne inizializza i valori
         */
        try {
            fw.append(stringBuilder.toString());
            fw.close();
        } catch (IOException e) {
            System.err.println("LOG DI ERRORE: tentativo di scrittura fallito");
            e.printStackTrace();
        }


    }


}
